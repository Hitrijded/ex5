/*!
 For this class use default package
*/
package default;
//!  Create a public class wiyh name Myclass
public class MyClass {

//! Public methods.
    / *! Public methods of sum, subtraction, multiplication, division, remainder of the division * /

/*! a function that outputs the sum of a and b */  
public int add(int a, int b){
return a + b;
}
/*! a function that outputs the adding of a and b */ 
public String add(String a, String b){
return a + b;
}
/*! a function that outputs the difference of a and b */ 
public int sub(int a, int b){
return a - b;
}
/*! a function that outputs the multiplication of a and b */ 
public int mult(int a, int b){
return a * b;
}
/*! a function that outputs the division of a and b */ 
public int div(int a, int b){
return a / b;
}
/*! a function that outputs remainder of the division  of a and b  */ 
public int mod(int a, int b){
return a%b;
}
/*! a function that outputs "Hello" and String name */ 
public String sayHello(String name){
return "Hello" + name;
}
}